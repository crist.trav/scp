package com.exabit.scp;

import com.exabit.scp.entidades.Parametro;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 * FXML Controller class
 *
 * @author cristhian
 */
public class VentanaLoginController implements Initializable {

    private Stage stgPadre;

    @FXML
    private TextField txfUsuarioSistema;
    @FXML
    private Button btnOkLogin;
    @FXML
    private Button btnCancelLogin;
    private static final Logger LOG = Logger.getLogger(VentanaLoginController.class.getName());
    @FXML
    private PasswordField pwfContrasenia;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void onActionOkLogin(ActionEvent event) {
        this.abrirVentanaPrincipal();
    }

    @FXML
    private void onActionTxfUsuario(ActionEvent event) {
        this.pwfContrasenia.requestFocus();
    }

    @FXML
    private void onActionPwfContrasenia(ActionEvent event) {
        this.abrirVentanaPrincipal();
    }

    private void abrirVentanaPrincipal() {
        try {
            FXMLLoader loader = new FXMLLoader();
            Stage stage = new Stage();
            Parent root = loader.load(getClass().getResourceAsStream("/fxml/VentanaPrincipal.fxml"));
            VentanaPrincipalController vprincContr = loader.getController();
            vprincContr.setUsuarioLogueado(this.txfUsuarioSistema.getText());
            Scene scene = new Scene(root);
            stage.setOnShown((e) -> {
                EntityManagerFactory emf = Persistence.createEntityManagerFactory("scpPU");
                EntityManager em = emf.createEntityManager();
                TypedQuery<Parametro> qimpre = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='impresora'", Parametro.class);
                List<Parametro> lstImpreBd = qimpre.getResultList();
                if (lstImpreBd.isEmpty()) {
                    Alert al = new Alert(Alert.AlertType.WARNING);
                    al.setTitle("Impresora");
                    al.setHeaderText("Ninguna impresora configurada");
                    al.setContentText("Acceda al menú Archivo > Opciones para configurar");
                    al.showAndWait();
                }
            });
            stage.setScene(scene);
            this.stgPadre.close();
            stage.show();
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al cargar interfaz principal", ex);
        }
    }

    public Stage getStgPadre() {
        return stgPadre;
    }

    public void setStgPadre(Stage stgPadre) {
        this.stgPadre = stgPadre;
    }

    @FXML
    private void onActionBtnCancelarLogin(ActionEvent event) {
        this.stgPadre.close();
    }

}
