package com.exabit.scp;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author cristhian
 */
public class VentanaLoginAdminController implements Initializable {
    
    private Stage stgPadre;

    @FXML
    private TextField txfUsuarioSistema;
    @FXML
    private Button btnOkLogin;
    @FXML
    private Button btnCancelLogin;
    private static final Logger LOG = Logger.getLogger(VentanaLoginAdminController.class.getName());
    @FXML
    private PasswordField pwfContrasenia;

    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void onActionOkLogin(ActionEvent event) {
        if(this.txfUsuarioSistema.getText().equals("admin") && this.pwfContrasenia.getText().equals("161953")){
            this.abrirVentanaPrincipal();
        }else{
            Alert al=new Alert(AlertType.ERROR);
            al.setTitle("Configuración");
            al.setContentText("Credenciales incorrectas");
            al.show();
        }
    }

    @FXML
    private void onActionTxfUsuario(ActionEvent event) {
        this.pwfContrasenia.requestFocus();
    }

    @FXML
    private void onActionPwfContrasenia(ActionEvent event) {
        if(this.txfUsuarioSistema.getText().equals("admin") && this.pwfContrasenia.getText().equals("161953")){
            this.abrirVentanaPrincipal();
        }else{
            Alert al=new Alert(AlertType.ERROR);
            al.setTitle("Configuración");
            al.setContentText("Credenciales incorrectas");
            al.show();
        }
    }
    
    
    private void abrirVentanaPrincipal(){
        try{
            FXMLLoader loader=new FXMLLoader();
            Stage stage=new Stage();
            Parent root = loader.load(getClass().getResourceAsStream("/fxml/VentanaOpciones.fxml"));
            VentanaOpcionesController vprincContr=loader.getController();
            vprincContr.setStgPadre(stage);
            Scene scene = new Scene(root);

            stage.setScene(scene);
            this.stgPadre.close();
            stage.show();
        }catch(Exception ex){
            LOG.log(Level.SEVERE, "Error al cargar interfaz principal", ex);
        }
    }

    public Stage getStgPadre() {
        return stgPadre;
    }

    public void setStgPadre(Stage stgPadre) {
        this.stgPadre = stgPadre;
    }

    @FXML
    private void onActionBtnCancelarLogin(ActionEvent event) {
        this.stgPadre.close();
    }
    
    
}
