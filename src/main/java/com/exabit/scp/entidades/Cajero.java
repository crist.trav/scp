package com.exabit.scp.entidades;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author traver
 */
@Entity
public class Cajero {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idCajero;

    @Column(length = 60)
    @Basic(optional = false)
    private String nombre;

    public Integer getIdCajero() {
        return this.idCajero;
    }

    public void setIdCajero(Integer idCajero) {
        this.idCajero = idCajero;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}