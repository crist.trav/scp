package com.exabit.scp.entidades;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author traver
 */
@Entity
public class MedioPago {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idMedioPago;

    @Column(length = 20)
    @Basic(optional = false)
    private String descripcion;

    public Integer getIdMedioPago() {
        return this.idMedioPago;
    }

    public void setIdMedioPago(Integer idMedioPago) {
        this.idMedioPago = idMedioPago;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}