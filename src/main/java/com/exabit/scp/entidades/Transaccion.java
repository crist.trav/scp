package com.exabit.scp.entidades;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Transaccion {

    @Id
    private Integer idTransaccion;

    @Basic
    private String categoria;

    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaHoraRegistro;

    @Basic
    private Integer total = 0;

    @ManyToOne(targetEntity = Cajero.class)
    private Cajero cajero;

    @ManyToOne(targetEntity = MedioPago.class)
    private MedioPago medioPago;

    public Integer getIdTransaccion() {
        return this.idTransaccion;
    }

    public void setIdTransaccion(Integer idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public String getCategoria() {
        return this.categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Calendar getFechaHoraRegistro() {
        return this.fechaHoraRegistro;
    }

    public void setFechaHoraRegistro(Calendar fechaHoraRegistro) {
        this.fechaHoraRegistro = fechaHoraRegistro;
    }

    public Integer getTotal() {
        return this.total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Cajero getCajero() {
        return this.cajero;
    }

    public void setCajero(Cajero cajero) {
        this.cajero = cajero;
    }

    public MedioPago getMedioPago() {
        return this.medioPago;
    }

    public void setMedioPago(MedioPago medioPago) {
        this.medioPago = medioPago;
    }

}