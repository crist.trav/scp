package com.exabit.scp;

import com.exabit.scp.entidades.Transaccion;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.layout.AnchorPane;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.swing.JRViewer;

/**
 * FXML Controller class
 *
 * @author cristhian
 */
public class VentanaInformeController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private final SwingNode swnInforme = new SwingNode();
    @FXML
    private AnchorPane apanePrincipal;
    private final ObjectProperty<JasperPrint> reporteProperty = new SimpleObjectProperty<>();
    @FXML
    private DatePicker dpkFechaDesde;
    @FXML
    private ComboBox<Integer> cmbHoraDesde;
    @FXML
    private ComboBox<Integer> cmbMinutoDesde;
    @FXML
    private DatePicker dpkFechaHasta;
    @FXML
    private ComboBox<Integer> cmbHoraHasta;
    @FXML
    private ComboBox<Integer> cmbMinutoHasta;
    
    private final EntityManagerFactory emf=Persistence.createEntityManagerFactory("scpPU");
    private static final Logger LOG = Logger.getLogger(VentanaInformeController.class.getName());
    

    public JasperPrint getReporteProperty() {
        return reporteProperty.get();
    }

    public void setReporteProperty(JasperPrint value) {
        reporteProperty.set(value);
    }

    public ObjectProperty reporteProperty() {
        return reporteProperty;
    }

    private final ChangeListener<JasperPrint> chLnrReporte = (ObservableValue<? extends JasperPrint> obs, JasperPrint oldValue, JasperPrint newValue) -> {
        if (newValue != null) {
            JRViewer visor = new JRViewer(newValue);
            this.swnInforme.setContent(visor);
        }
    };

    private final ObservableList<Integer> lstHoras = FXCollections.observableArrayList();
    private final ObservableList<Integer> lstMinutos = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        for (int i = 0; i < 24; i++) {
            lstHoras.add(i);
        }
        for (int i = 0; i < 60; i = i + 1) {
            this.lstMinutos.add(i);
        }
        this.cmbHoraDesde.setItems(lstHoras);
        this.cmbHoraHasta.setItems(lstHoras);
        this.cmbMinutoDesde.setItems(this.lstMinutos);
        this.cmbMinutoHasta.setItems(this.lstMinutos);

        this.cmbHoraDesde.getSelectionModel().selectFirst();
        this.cmbMinutoDesde.getSelectionModel().selectFirst();
        this.cmbHoraHasta.getSelectionModel().selectLast();
        this.cmbMinutoHasta.getSelectionModel().selectLast();

        this.dpkFechaDesde.setValue(LocalDate.now());
        this.dpkFechaHasta.setValue(LocalDate.now());
        AnchorPane.setTopAnchor(swnInforme, 115.0);
        AnchorPane.setBottomAnchor(swnInforme, 0.0);
        AnchorPane.setRightAnchor(swnInforme, 0.0);
        AnchorPane.setLeftAnchor(swnInforme, 0.0);
        this.apanePrincipal.getChildren().add(swnInforme);
        this.reporteProperty.addListener(chLnrReporte);
    }

    @FXML
    private void onActionBtnActualizar(ActionEvent event) {
        this.cargarReporte();
    }
    
    private Date getDateDesde(){
        Calendar cDesde=new GregorianCalendar();
        cDesde.set(Calendar.SECOND, 0);
        cDesde.set(Calendar.MILLISECOND, 0);
        cDesde.set(Calendar.HOUR_OF_DAY, this.cmbHoraDesde.getSelectionModel().getSelectedItem());
        cDesde.set(Calendar.MINUTE, this.cmbMinutoDesde.getSelectionModel().getSelectedItem());
        cDesde.set(Calendar.YEAR, this.dpkFechaDesde.getValue().getYear());
        cDesde.set(Calendar.MONTH, (this.dpkFechaDesde.getValue().getMonthValue()-1));
        cDesde.set(Calendar.DAY_OF_MONTH, this.dpkFechaDesde.getValue().getDayOfMonth());
        Date dDesde=new Date(cDesde.getTimeInMillis());
        return dDesde;
    }
    private Date getDateHasta(){
        Calendar cHasta=new GregorianCalendar();
        cHasta.set(Calendar.SECOND, 59);
        cHasta.set(Calendar.MILLISECOND, 999);
        cHasta.set(Calendar.HOUR_OF_DAY, this.cmbHoraHasta.getSelectionModel().getSelectedItem());
        cHasta.set(Calendar.MINUTE, this.cmbMinutoHasta.getSelectionModel().getSelectedItem());
        cHasta.set(Calendar.YEAR, this.dpkFechaHasta.getValue().getYear());
        cHasta.set(Calendar.MONTH, (this.dpkFechaHasta.getValue().getMonthValue()-1));
        cHasta.set(Calendar.DAY_OF_MONTH, this.dpkFechaHasta.getValue().getDayOfMonth());
        Date dHasta=new Date(cHasta.getTimeInMillis());
        return dHasta;
    }

    private void cargarReporte() {
        List<ResumenTicketsModeloDatos> lstDatosReporte = new ArrayList<>();
        EntityManager em = this.emf.createEntityManager();
        for (int i = 1; i <= 9; i++) {
            TypedQuery<Transaccion> q = em.createQuery("SELECT t FROM Transaccion t WHERE t.categoria=:cat AND t.fechaHoraRegistro>=:fdesde AND t.fechaHoraRegistro<=:fhasta", Transaccion.class);
            q.setParameter("cat", "Cat" + i);
            q.setParameter("fdesde", this.getDateDesde());
            q.setParameter("fhasta", this.getDateHasta());
            List<Transaccion> lstRes = q.getResultList();
            if (!lstRes.isEmpty()) {
                ResumenTicketsModeloDatos r = new ResumenTicketsModeloDatos();
                r.setCategoria("Cat" + i);
                r.setCantidad(lstRes.size());
                lstDatosReporte.add(r);
            }
        }
        try {
            Map<String, Object> param = new HashMap<>();
            param.put("fhDesde", this.getDateDesde());
            param.put("fhHasta", this.getDateHasta());
            JasperPrint jprint = null;
            JasperReport reporte = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("/jasper/ResumenTickets.jasper"));
            JRBeanCollectionDataSource datos = new JRBeanCollectionDataSource(lstDatosReporte);
            jprint = JasperFillManager.fillReport(reporte, param, datos);
            this.reporteProperty.setValue(jprint);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al cargar reporte", ex);
        }
    }

}
