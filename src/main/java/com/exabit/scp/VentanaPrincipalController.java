package com.exabit.scp;

import com.exabit.scp.entidades.Cajero;
import com.exabit.scp.entidades.MedioPago;
import com.exabit.scp.entidades.Parametro;
import com.exabit.scp.entidades.Transaccion;
import com.exabit.scp.impresion.TicketController;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.print.PageLayout;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.PrinterName;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimplePrintServiceExporterConfiguration;
import net.sf.jasperreports.swing.JRViewer;

/**
 *
 * @author cristhian
 */
public class VentanaPrincipalController implements Initializable {

    private static final Logger LOG = Logger.getLogger(VentanaPrincipalController.class.getName());
    @FXML
    private AnchorPane apanePrincipal;
    @FXML
    private GridPane grpNroTrans;
    @FXML
    private TextField txfNroInicialTransaccion;
    @FXML
    private ComboBox<String> cmbClaseOperador;
    @FXML
    private ComboBox<String> cmbFormaPago;
    @FXML
    private TextField txfMontoPagar;
    @FXML
    private GridPane gpFormulario;
    @FXML
    private Button btnReiniciarTransaccion;
    @FXML
    private Button btnFinalizarTrans;
    @FXML
    private Separator spFormulario;

    private AnchorPane apaneCat = null;
    private AnchorPane apanePago = null;
    private VentanaCatController contrCateg = null;
    private VentanaFormaPagoController contrFormaPago = null;

    private final ObservableList<String> lstFormaPago = FXCollections.observableArrayList();
    private final ObservableList<String> lstClaseOperador = FXCollections.observableArrayList();

    private String modoActualPanel = "seleccionCategoria";

    private String usuarioLogueado = "";

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("scpPU");

    private final StringProperty carrilActual = new SimpleStringProperty();
    private final StringProperty nombrePuestoActual = new SimpleStringProperty();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        EntityManager em = this.emf.createEntityManager();
        TypedQuery<Parametro> q = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='carril'", Parametro.class);
        List<Parametro> lstPars = q.getResultList();
        if (!lstPars.isEmpty()) {
            this.carrilActual.setValue(lstPars.get(0).getValor());
        }
        TypedQuery<Parametro> qnp = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='nombrepuesto'", Parametro.class);
        List<Parametro> lstNombrePuesto = qnp.getResultList();
        if (!lstNombrePuesto.isEmpty()) {
            this.nombrePuestoActual.setValue(lstNombrePuesto.get(0).getValor());
        }

        lstFormaPago.add("Efectivo");
        lstFormaPago.add("Bono");
        lstFormaPago.add("Exento");
        this.cmbFormaPago.setItems(lstFormaPago);

        for (int i = 1; i <= 9; i++) {
            this.lstClaseOperador.add("Cat" + i);
        }
        this.cmbClaseOperador.setItems(this.lstClaseOperador);

        try {
            FXMLLoader loader = new FXMLLoader();
            apaneCat = loader.load(getClass().getResourceAsStream("/fxml/VentanaCat.fxml"));
            this.contrCateg = loader.getController();

            FXMLLoader loaderPago = new FXMLLoader();
            apanePago = loaderPago.load(getClass().getResourceAsStream("/fxml/VentanaFormaPago.fxml"));
            this.contrFormaPago = loaderPago.getController();

            this.inicializarBotonesCategorias();
            this.inicializarBotonesFormaPago();

            AnchorPane.setTopAnchor(apaneCat, 30.0);
            AnchorPane.setBottomAnchor(apaneCat, 30.0);
            AnchorPane.setLeftAnchor(apaneCat, 0.0);
            AnchorPane.setRightAnchor(apaneCat, 500.0);
            this.apanePrincipal.getChildren().add(apaneCat);

            AnchorPane.setTopAnchor(this.apanePago, 30.0);
            AnchorPane.setBottomAnchor(this.apanePago, 30.0);
            AnchorPane.setLeftAnchor(this.apanePago, 0.0);
            AnchorPane.setRightAnchor(this.apanePago, 500.0);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al cargar interfaz de seleccion de categoria", ex);
        }

        TypedQuery<Parametro> tqmostrarnrotrans = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='mostrarnrotransaccionprincipal'", Parametro.class);
        List<Parametro> lstmostrarnrotrans = tqmostrarnrotrans.getResultList();
        if (!lstmostrarnrotrans.isEmpty()) {
            String mostrarnrotrans = lstmostrarnrotrans.get(0).getValor();
            if (Boolean.parseBoolean(mostrarnrotrans)) {
                this.mostrarGPNroTransaccion();
            } else {
                this.ocultarGPNroTransaccion();
            }
        } else {
            this.ocultarGPNroTransaccion();
        }

        TypedQuery<Parametro> tqpn = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='nrotransaccion'", Parametro.class);
        List<Parametro> lstpnro = tqpn.getResultList();
        if (!lstpnro.isEmpty()) {
            String nrotrans = lstpnro.get(0).getValor();
            this.txfNroInicialTransaccion.setText(nrotrans);
        }

        this.txfNroInicialTransaccion.focusedProperty().addListener((ObservableValue<? extends Boolean> obs, Boolean oldvalue, Boolean newvalue) -> {
            if (!newvalue) {
                try {
                    Integer nrosig = Integer.parseInt(this.txfNroInicialTransaccion.getText());

                    if (this.nroTransaccionUsado()) {
                        Alert a = new Alert(AlertType.ERROR);
                        a.setTitle("Error al guardar numero de transacción");
                        a.setContentText("El número de transacción " + nrosig + " ya se está registrado.");
                        a.show();

                        this.cargarNroTransaccion();
                    }

                    em.getTransaction().begin();
                    Parametro pnro = new Parametro();
                    pnro.setClave("nrotransaccion");
                    pnro.setValor(this.txfNroInicialTransaccion.getText());
                    em.merge(pnro);
                    em.getTransaction().commit();

                } catch (NumberFormatException ex) {
                    LOG.log(Level.SEVERE, "Error al convertir nro de transaccion a INT", ex);

                    this.cargarNroTransaccion();
                }
            }
        });

    }

    private boolean nroTransaccionUsado() {
        EntityManager em = this.emf.createEntityManager();
        Integer nrosig = Integer.parseInt(this.txfNroInicialTransaccion.getText());

        TypedQuery<Transaccion> tqtrans = em.createQuery("SELECT t FROM Transaccion t WHERE t.idTransaccion=:id", Transaccion.class);
        tqtrans.setParameter("id", nrosig);
        List<Transaccion> lsttrans = tqtrans.getResultList();
        if (lsttrans.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private void inicializarBotonesCategorias() {
        EntityManager em = this.emf.createEntityManager();
        if (this.contrCateg != null) {
            this.contrCateg.getBotonCat(1).setOnAction((evt) -> {
                TypedQuery<Parametro> qp1 = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat1'", Parametro.class);
                this.txfMontoPagar.setText(qp1.getSingleResult().getValor());
                this.cmbClaseOperador.getSelectionModel().select(this.contrCateg.getBotonCat(1).getText());
                this.cmbClaseOperador.setDisable(true);
                this.colocarPanelPago();
            });
            this.contrCateg.getBotonCat(2).setOnAction((evt) -> {
                TypedQuery<Parametro> qp = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat2'", Parametro.class);
                this.txfMontoPagar.setText(qp.getSingleResult().getValor());
                this.cmbClaseOperador.getSelectionModel().select(this.contrCateg.getBotonCat(2).getText());
                this.cmbClaseOperador.setDisable(true);
                this.colocarPanelPago();
            });
            this.contrCateg.getBotonCat(3).setOnAction((evt) -> {
                TypedQuery<Parametro> qp = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat3'", Parametro.class);
                this.txfMontoPagar.setText(qp.getSingleResult().getValor());
                this.cmbClaseOperador.getSelectionModel().select(this.contrCateg.getBotonCat(3).getText());
                this.cmbClaseOperador.setDisable(true);
                this.colocarPanelPago();
            });
            this.contrCateg.getBotonCat(4).setOnAction((evt) -> {
                TypedQuery<Parametro> qp = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat4'", Parametro.class);
                this.txfMontoPagar.setText(qp.getSingleResult().getValor());
                this.cmbClaseOperador.getSelectionModel().select(this.contrCateg.getBotonCat(4).getText());
                this.cmbClaseOperador.setDisable(true);
                this.colocarPanelPago();
            });
            this.contrCateg.getBotonCat(5).setOnAction((evt) -> {
                TypedQuery<Parametro> qp = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat5'", Parametro.class);
                this.txfMontoPagar.setText(qp.getSingleResult().getValor());
                this.cmbClaseOperador.getSelectionModel().select(this.contrCateg.getBotonCat(5).getText());
                this.cmbClaseOperador.setDisable(true);
                this.colocarPanelPago();
            });
            this.contrCateg.getBotonCat(6).setOnAction((evt) -> {
                TypedQuery<Parametro> qp = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat6'", Parametro.class);
                this.txfMontoPagar.setText(qp.getSingleResult().getValor());
                this.cmbClaseOperador.getSelectionModel().select(this.contrCateg.getBotonCat(6).getText());
                this.cmbClaseOperador.setDisable(true);
                this.colocarPanelPago();
            });
            this.contrCateg.getBotonCat(7).setOnAction((evt) -> {
                TypedQuery<Parametro> qp = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat7'", Parametro.class);
                this.txfMontoPagar.setText(qp.getSingleResult().getValor());
                this.cmbClaseOperador.getSelectionModel().select(this.contrCateg.getBotonCat(7).getText());
                this.cmbClaseOperador.setDisable(true);
                this.colocarPanelPago();
            });
            this.contrCateg.getBotonCat(8).setOnAction((evt) -> {
                TypedQuery<Parametro> qp = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat8'", Parametro.class);
                this.txfMontoPagar.setText(qp.getSingleResult().getValor());
                this.cmbClaseOperador.getSelectionModel().select(this.contrCateg.getBotonCat(8).getText());
                this.cmbClaseOperador.setDisable(true);
                this.colocarPanelPago();
            });
            this.contrCateg.getBotonCat(9).setOnAction((evt) -> {
                TypedQuery<Parametro> qp = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat9'", Parametro.class);
                this.txfMontoPagar.setText(qp.getSingleResult().getValor());
                this.cmbClaseOperador.getSelectionModel().select(this.contrCateg.getBotonCat(9).getText());
                this.cmbClaseOperador.setDisable(true);
                this.colocarPanelPago();
            });
        } else {
            System.out.println("controlador categorias nulo: ");
        }
    }

    private void inicializarBotonesFormaPago() {
        if (this.contrFormaPago != null) {
            this.contrFormaPago.getBotonEfectivo().setOnAction((evt) -> {
                this.cmbFormaPago.getSelectionModel().select("Efectivo");
                this.btnFinalizarTrans.setDisable(false);
            });
            this.contrFormaPago.getBotonBono().setOnAction((evt) -> {
                this.cmbFormaPago.getSelectionModel().select("Bono");
                this.btnFinalizarTrans.setDisable(false);
            });
            this.contrFormaPago.getBotonExento().setOnAction((evt) -> {
                this.cmbFormaPago.getSelectionModel().select("Exento");
                this.btnFinalizarTrans.setDisable(false);
            });
        }
    }

    private void colocarPanelPago() {
        this.apanePrincipal.getChildren().remove(this.apaneCat);
        this.apanePrincipal.getChildren().add(this.apanePago);
        this.cmbFormaPago.setDisable(false);
        this.modoActualPanel = "seleccionFormaPago";
    }

    private void colocarPanelCateg() {
        this.apanePrincipal.getChildren().remove(this.apanePago);
        if (!this.apanePrincipal.getChildren().contains(this.apaneCat)) {
            this.apanePrincipal.getChildren().add(this.apaneCat);
        }

        this.cmbClaseOperador.setDisable(false);
        this.modoActualPanel = "seleccionCateg";
    }

    @FXML
    private void onActionBtnReiniciarTrans(ActionEvent event) {
        this.reiniciarTransaccion();
    }

    private void reiniciarTransaccion() {
        this.cmbClaseOperador.getSelectionModel().clearSelection();
        this.cmbFormaPago.getSelectionModel().clearSelection();
        this.txfMontoPagar.setText("0");
        this.btnFinalizarTrans.setDisable(true);
        this.colocarPanelCateg();
    }

    @FXML
    private void onActionBtnFinalizarTrans(ActionEvent event) {
        if (this.nroTransaccionUsado()) {
            Alert al = new Alert(AlertType.ERROR);
            al.setTitle("Finalizar transacción");
            al.setContentText("Número de transacción ya registrado.");
            al.show();
        } else {
            try {
                EntityManager em = emf.createEntityManager();

                TypedQuery<Parametro> q = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='carril'", Parametro.class);
                List<Parametro> lstPars = q.getResultList();
                if (!lstPars.isEmpty()) {
                    this.carrilActual.setValue(lstPars.get(0).getValor());
                }
                TypedQuery<Parametro> qnp = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='nombrepuesto'", Parametro.class);
                List<Parametro> lstNombrePuesto = qnp.getResultList();
                if (!lstNombrePuesto.isEmpty()) {
                    this.nombrePuestoActual.setValue(lstNombrePuesto.get(0).getValor());
                }

                Transaccion ts = new Transaccion();
                ts.setCategoria(this.cmbClaseOperador.getSelectionModel().getSelectedItem());
                ts.setFechaHoraRegistro(new GregorianCalendar());
                ts.setIdTransaccion(this.getUltimaTransaccion());
                ts.setCajero(em.find(Cajero.class, 1));
                ts.setCategoria(this.cmbClaseOperador.getSelectionModel().getSelectedItem());
                if (this.cmbFormaPago.getSelectionModel().getSelectedItem().equals("Efectivo")) {
                    ts.setMedioPago(em.find(MedioPago.class, 1));
                } else if (this.cmbFormaPago.getSelectionModel().getSelectedItem().equals("Bono")) {
                    ts.setMedioPago(em.find(MedioPago.class, 2));
                } else {
                    ts.setMedioPago(em.find(MedioPago.class, 3));
                }
                DecimalFormat df = new DecimalFormat("#,###");
                try {
                    Long lt = (Long) df.parse(this.txfMontoPagar.getText());
                    ts.setTotal(lt.intValue());
                } catch (ParseException ex) {
                    LOG.log(Level.SEVERE, "Error al convertir a numero el monto", ex);
                    ts.setTotal(0);
                }

                em.getTransaction().begin();
                em.persist(ts);
                Parametro pnro = new Parametro();
                pnro.setClave("nrotransaccion");
                pnro.setValor(String.valueOf(ts.getIdTransaccion() + 1));
                em.merge(pnro);
                em.getTransaction().commit();

                List<TicketModeloDatos> lstDatosTicket = new LinkedList<>();
                TicketModeloDatos tm = new TicketModeloDatos();
                tm.setCodTransaccion(ts.getIdTransaccion().toString());
                tm.setCategoria(this.cmbClaseOperador.getSelectionModel().getSelectedItem());
                tm.setCarril(this.carrilActual.getValue());
                tm.setFormaPago(this.cmbFormaPago.getSelectionModel().getSelectedItem());
                tm.setMontoPago(this.txfMontoPagar.getText());
                System.out.println("Monto pago:  " + tm.getMontoPago());
                lstDatosTicket.add(tm);
                Map<String, Object> param = new HashMap<>();
                param.put("nombrePuesto", this.nombrePuestoActual.getValue());
                param.put("cajero", this.usuarioLogueado);
                JasperPrint jprint = null;
                JasperReport reporte = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("/jasper/TicketPeaje2.jasper"));
                JRBeanCollectionDataSource datos = new JRBeanCollectionDataSource(lstDatosTicket);
                jprint = JasperFillManager.fillReport(reporte, param, datos);
                //JRViewer jrviewer=new JRViewer(jprint);
                //JasperViewer.viewReport(jprint);
                //swNode.setContent(jrviewer);
                //jprintActual=jprint;

                //JasperPrintManager.printReport(jprint, false);
                /*Alert al=new Alert(AlertType.INFORMATION);
            al.setTitle("Transacción");
            al.setContentText("Transacción exitosa");
            al.show();*/
                Boolean vistaPrevia = false;
                TypedQuery<Parametro> qvista = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='vistaprevia'", Parametro.class);
                List<Parametro> lstVistaPrevia = qvista.getResultList();
                if (!lstVistaPrevia.isEmpty()) {
                    vistaPrevia = Boolean.parseBoolean(lstVistaPrevia.get(0).getValor());
                }
                if (vistaPrevia) {
                    this.vistaPreviaImpresion(jprint);
                } else {
                    //this.impresionDirecta(jprint);
                    this.impresionDirectaFX(ts, this.usuarioLogueado, this.carrilActual.getValue(), this.cmbFormaPago.getSelectionModel().getSelectedItem(), tm.getMontoPago(), this.nombrePuestoActual.getValue());
                }
                this.reiniciarTransaccion();
                this.cargarNroTransaccion();
            } catch (JRException ex) {
                LOG.log(Level.SEVERE, "Error al mostrar informe ticket", ex);
            }
        }
    }

    private void vistaPreviaImpresion(JasperPrint jp) {
        JRViewer visor = new JRViewer(jp);
        AnchorPane apaneVisor = new AnchorPane();
        apaneVisor.setPrefWidth(800);
        apaneVisor.setPrefHeight(600);
        SwingNode swnInforme = new SwingNode();
        AnchorPane.setTopAnchor(swnInforme, 0.0);
        AnchorPane.setBottomAnchor(swnInforme, 0.0);
        AnchorPane.setLeftAnchor(swnInforme, 0.0);
        AnchorPane.setRightAnchor(swnInforme, 0.0);
        apaneVisor.getChildren().add(swnInforme);
        swnInforme.setContent(visor);
        Stage stg = new Stage();
        Scene sce = new Scene(apaneVisor);
        stg.setScene(sce);
        stg.setTitle("Vista Previa Ticket");
        stg.show();
    }

    private void impresionDirectaFX(Transaccion t, String cajero, String carril, String metodoPago, String montoPago, String puesto) {
        Node ticketNode = null;
        try {
            FXMLLoader loader = new FXMLLoader();
            ticketNode = loader.load(getClass().getResourceAsStream("/fxml/impresion/ticket.fxml"));
            TicketController tickContr = loader.getController();
            tickContr.cargarDatos(t, cajero, carril, metodoPago, montoPago, puesto);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Error al cargar ticket fx", ex);
        }

        EntityManager em = this.emf.createEntityManager();

        Double margenIzq = 0.0;
        String umMargenIzq = "mm";

        TypedQuery<Parametro> qmargenizq = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='margenizq'", Parametro.class);
        List<Parametro> lstMargenIzq = qmargenizq.getResultList();
        if (!lstMargenIzq.isEmpty()) {
            margenIzq = Double.parseDouble(lstMargenIzq.get(0).getValor());
        }

        TypedQuery<Parametro> tqunidadizq = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='ummargenizq'", Parametro.class);
        List<Parametro> lstumizq = tqunidadizq.getResultList();
        if (!lstumizq.isEmpty()) {
            umMargenIzq = lstumizq.get(0).getValor();
        }

        TypedQuery<Parametro> qimpre = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='impresora'", Parametro.class);
        List<Parametro> lstImpreBd = qimpre.getResultList();
        String nombreImpresora = lstImpreBd.get(0).getValor();

        Printer impresora = null;

        Set<Printer> lstPrinters = Printer.getAllPrinters();
        for (Printer p : lstPrinters) {
            System.out.println("Impresora: " + p.getName());
            if (p.getName().equals(nombreImpresora)) {
                impresora = p;
                break;
            }
        }
        if (impresora != null && ticketNode != null) {
            PrinterJob printerJob = PrinterJob.createPrinterJob(impresora);
            Double lMarginDots = 0.0;
            if (margenIzq > 0) {
                Double lMarginInch;
                if (umMargenIzq.equals("cm")) {
                    lMarginInch=margenIzq/2.54;
                } else {
                    lMarginInch=margenIzq/25.4;
                }
                lMarginDots=lMarginInch/0.01388888888;
            }
            PageLayout layout = impresora.createPageLayout(impresora.getDefaultPageLayout().getPaper(), impresora.getDefaultPageLayout().getPageOrientation(), lMarginDots, 0, 0, 0);
            printerJob.getJobSettings().setPageLayout(layout);
            //ticketNode.getTransforms().add(new Scale(0.625, 0.625));
            boolean impreso = printerJob.printPage(ticketNode);
            if (impreso) {
                printerJob.endJob();
            } else {
                System.out.println("Error al imprimir");
            }
        } else {
            Alert al = new Alert(AlertType.ERROR);
            al.setTitle("Impresora");
            al.setContentText("Impresora no encontrada");
            al.show();
        }
    }

    private void impresionDirecta(JasperPrint jp) {
        EntityManager em = this.emf.createEntityManager();
        TypedQuery<Parametro> qimpre = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='impresora'", Parametro.class);
        List<Parametro> lstImpreBd = qimpre.getResultList();
        String nombreImpresora = lstImpreBd.get(0).getValor();

        System.out.println("Nombre impresora: " + nombreImpresora);

        if (!lstImpreBd.isEmpty()) {

            try {

                PrintServiceAttributeSet psaSet = new HashPrintServiceAttributeSet();
                PrintRequestAttributeSet praSet = new HashPrintRequestAttributeSet();
                psaSet.add(new PrinterName(nombreImpresora, null));
                JRPrintServiceExporter printExporter = new JRPrintServiceExporter();
                printExporter.setExporterInput(new SimpleExporterInput(jp));
                SimplePrintServiceExporterConfiguration printConfiguration = new SimplePrintServiceExporterConfiguration();
                printConfiguration.setPrintRequestAttributeSet(praSet);
                printConfiguration.setPrintServiceAttributeSet(psaSet);
                printConfiguration.setDisplayPageDialog(false);
                printConfiguration.setDisplayPrintDialog(false);
                printExporter.setConfiguration(printConfiguration);

                printExporter.exportReport();
            } catch (Exception ex) {
                LOG.log(Level.SEVERE, "Error al imprimir", ex);
            }
        } else {
            try {
                JasperPrintManager.printReport(jp, false);
            } catch (Exception ex) {
                LOG.log(Level.SEVERE, "Error al imprimir", ex);
            }
        }
    }

    public String getUsuarioLogueado() {
        return usuarioLogueado;
    }

    public void setUsuarioLogueado(String usuarioLogueado) {
        this.usuarioLogueado = usuarioLogueado;
    }

    @FXML
    private void onActionMenuOpciones(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            Stage stage = new Stage();
            Parent root = loader.load(getClass().getResourceAsStream("/fxml/VentanaLoginAdmin.fxml"));
            VentanaLoginAdminController vOpcionesContr = loader.getController();
            vOpcionesContr.setStgPadre(stage);
            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.setTitle("Autorización");
            stage.show();
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al cargar interfaz principal", ex);
        }
    }

    @FXML
    private void onActionMenuResumenTickets(ActionEvent event) {
        /*List<ResumenTicketsModeloDatos> lstDatosReporte = new ArrayList<>();
        EntityManager em = this.emf.createEntityManager();
        for (int i = 1; i <= 9; i++) {
            TypedQuery<Transaccion> q = em.createQuery("SELECT t FROM Transaccion t WHERE t.categoria=:cat", Transaccion.class);
            q.setParameter("cat", "Cat" + i);
            List<Transaccion> lstRes = q.getResultList();
            if (!lstRes.isEmpty()) {
                ResumenTicketsModeloDatos r = new ResumenTicketsModeloDatos();
                r.setCategoria("Cat" + i);
                r.setCantidad(lstRes.size());
                lstDatosReporte.add(r);
            }
        }*/
        try {
            Stage stage = new Stage();
            stage.setWidth(1024);
            stage.setHeight(768);
            FXMLLoader loader = new FXMLLoader();
            Parent root = loader.load(getClass().getResourceAsStream("/fxml/VentanaInforme.fxml"));
            VentanaInformeController vinfContr = loader.getController();
            //vlogContr.setStgPadre(stage);
            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.setTitle("Resumen de tickets");
            /*Map<String, Object> param = new HashMap<>();
            JasperPrint jprint = null;
            JasperReport reporte = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("/scp/reportes/ResumenTickets.jasper"));
            JRBeanCollectionDataSource datos = new JRBeanCollectionDataSource(lstDatosReporte);
            jprint = JasperFillManager.fillReport(reporte, param, datos);*/
            //JRViewer jrviewer=new JRViewer(jprint);
            //JasperViewer.viewReport(jprint);
            //swNode.setContent(jrviewer);
            //jprintActual=jprint;
            //vinfContr.setReporteProperty(jprint);
            stage.show();
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al cargar ventana de informes", ex);
        }
    }

    private int getUltimaTransaccion() {
        int nro = 1;
        EntityManager em = emf.createEntityManager();
        TypedQuery<Parametro> tqpn = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='nrotransaccion'", Parametro.class);
        List<Parametro> lstpnro = tqpn.getResultList();
        if (!lstpnro.isEmpty()) {
            String nrotrans = lstpnro.get(0).getValor();
            nro = Integer.parseInt(nrotrans);
        }
        return nro;
    }

    private void mostrarGPNroTransaccion() {
        if (!this.apanePrincipal.getChildren().contains(this.grpNroTrans)) {
            this.apanePrincipal.getChildren().add(this.grpNroTrans);
            AnchorPane.setTopAnchor(this.gpFormulario, 85.0);
            this.apanePrincipal.getChildren().add(this.spFormulario);
        }
    }

    private void ocultarGPNroTransaccion() {
        if (this.apanePrincipal.getChildren().contains(this.grpNroTrans)) {
            this.apanePrincipal.getChildren().remove(this.grpNroTrans);
            AnchorPane.setTopAnchor(this.gpFormulario, 40.0);
            this.apanePrincipal.getChildren().remove(this.spFormulario);
        }
    }

    private void cargarNroTransaccion() {
        EntityManager em = this.emf.createEntityManager();
        TypedQuery<Parametro> tqnrot = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='nrotransaccion'", Parametro.class);
        List<Parametro> lstnt = tqnrot.getResultList();
        if (!lstnt.isEmpty()) {
            String nrotrans = lstnt.get(0).getValor();
            this.txfNroInicialTransaccion.setText(nrotrans);
        }
    }

}
