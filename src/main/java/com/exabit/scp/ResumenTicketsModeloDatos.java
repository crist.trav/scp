package com.exabit.scp;

/**
 *
 * @author cristhian
 */
public class ResumenTicketsModeloDatos {
    
    private String categoria;

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    private Integer cantidad;

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
        
    }

    
}
