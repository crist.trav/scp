package com.exabit.scp;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

/**
 * FXML Controller class
 *
 * @author cristhian
 */
public class VentanaFormaPagoController implements Initializable {

    @FXML
    private Button btnEfectivo;
    @FXML
    private Button btnBono;
    @FXML
    private Button btnExento;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public Button getBotonEfectivo(){
        return this.btnEfectivo;
    }
    
    public Button getBotonBono(){
        return this.btnBono;
    }
    
    public Button getBotonExento(){
        return this.btnExento;
    }
    
}
