package com.exabit.scp;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *
 * @author cristhian
 */
public class VentanaCatController implements Initializable {
    
    private Label label;
    @FXML
    private Button btnCat1;
    @FXML
    private Button btnCat2;
    @FXML
    private Button btnCat3;
    @FXML
    private Button btnCat4;
    @FXML
    private Button btnCat5;
    @FXML
    private Button btnCat6;
    @FXML
    private Button btnCat7;
    @FXML
    private Button btnCat8;
    @FXML
    private Button btnCat9;
  
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public Button getBotonCat(int nroCat){
        switch(nroCat){
            case 1:
                return this.btnCat1;
            case 2:
                return this.btnCat2;
            case 3:
                return this.btnCat3;
            case 4:
                return this.btnCat4;
            case 5:
                return this.btnCat5;
            case 6:
                return this.btnCat6;
            case 7:
                return this.btnCat7;
            case 8:
                return this.btnCat8;
            case 9:
                return this.btnCat9;
            default:
                return null;
        }
    }
    
}
