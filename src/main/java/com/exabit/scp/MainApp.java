package com.exabit.scp;

import com.exabit.scp.entidades.Cajero;
import com.exabit.scp.entidades.MedioPago;
import com.exabit.scp.entidades.Parametro;
import java.util.List;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class MainApp extends Application {

    @Override
    public void start(Stage stage) throws Exception {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("scpPU");
        EntityManager em = emf.createEntityManager();

        TypedQuery<Parametro> q = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='carril'", Parametro.class);
        List<Parametro> lstPars = q.getResultList();
        em.getTransaction().begin();
        if (lstPars.isEmpty()) {
            Parametro carril = new Parametro();
            carril.setClave("carril");
            carril.setValor("1");
            em.persist(carril);
        }
        TypedQuery<Parametro> qnp = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='nombrepuesto'", Parametro.class);
        List<Parametro> lstNombrePuesto = qnp.getResultList();

        if (lstNombrePuesto.isEmpty()) {
            Parametro nombrepuesto = new Parametro();
            nombrepuesto.setClave("nombrepuesto");
            nombrepuesto.setValor("Puesto 1");
            em.persist(nombrepuesto);
        }

        TypedQuery<Parametro> qp1 = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat1'", Parametro.class);
        List<Parametro> lstPrecioCat1 = qp1.getResultList();
        if (lstPrecioCat1.isEmpty()) {
            Parametro preCat1 = new Parametro();
            preCat1.setClave("preciocat1");
            preCat1.setValor("5.000");
            em.persist(preCat1);
        }
        TypedQuery<Parametro> qp2 = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat2'", Parametro.class);
        List<Parametro> lstPrecioCat2 = qp2.getResultList();
        if (lstPrecioCat2.isEmpty()) {
            Parametro preCat2 = new Parametro();
            preCat2.setClave("preciocat2");
            preCat2.setValor("10.000");
            em.persist(preCat2);
        }
        TypedQuery<Parametro> qp3 = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat3'", Parametro.class);
        List<Parametro> lstPrecioCat3 = qp3.getResultList();
        if (lstPrecioCat3.isEmpty()) {
            Parametro preCat3 = new Parametro();
            preCat3.setClave("preciocat3");
            preCat3.setValor("15.000");
            em.persist(preCat3);
        }
        TypedQuery<Parametro> qp4 = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat4'", Parametro.class);
        List<Parametro> lstPrecioCat4 = qp4.getResultList();
        if (lstPrecioCat4.isEmpty()) {
            Parametro preCat4 = new Parametro();
            preCat4.setClave("preciocat4");
            preCat4.setValor("20.000");
            em.persist(preCat4);
        }
        TypedQuery<Parametro> qp5 = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat5'", Parametro.class);
        List<Parametro> lstPrecioCat5 = qp5.getResultList();
        if (lstPrecioCat5.isEmpty()) {
            Parametro preCat5 = new Parametro();
            preCat5.setClave("preciocat5");
            preCat5.setValor("20.000");
            em.persist(preCat5);
        }
        TypedQuery<Parametro> qp6 = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat6'", Parametro.class);
        List<Parametro> lstPrecioCat6 = qp6.getResultList();
        if (lstPrecioCat6.isEmpty()) {
            Parametro preCat6 = new Parametro();
            preCat6.setClave("preciocat6");
            preCat6.setValor("20.000");
            em.persist(preCat6);
        }
        TypedQuery<Parametro> qp7 = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat7'", Parametro.class);
        List<Parametro> lstPrecioCat7 = qp7.getResultList();
        if (lstPrecioCat7.isEmpty()) {
            Parametro preCat7 = new Parametro();
            preCat7.setClave("preciocat7");
            preCat7.setValor("10.000");
            em.persist(preCat7);
        }
        TypedQuery<Parametro> qp8 = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat8'", Parametro.class);
        List<Parametro> lstPrecioCat8 = qp8.getResultList();
        if (lstPrecioCat8.isEmpty()) {
            Parametro preCat8 = new Parametro();
            preCat8.setClave("preciocat8");
            preCat8.setValor("10.000");
            em.persist(preCat8);
        }
        TypedQuery<Parametro> qp9 = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat9'", Parametro.class);
        List<Parametro> lstPrecioCat9 = qp9.getResultList();
        if (lstPrecioCat9.isEmpty()) {
            Parametro preCat9 = new Parametro();
            preCat9.setClave("preciocat9");
            preCat9.setValor("20.000");
            em.persist(preCat9);
        }

        TypedQuery<Parametro> tqunidadizq = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='ummargenizq'", Parametro.class);
        List<Parametro> lstumizq = tqunidadizq.getResultList();
        if (lstumizq.isEmpty()) {
            Parametro pUmMargenIzq = new Parametro();
            pUmMargenIzq.setClave("ummargenizq");
            pUmMargenIzq.setValor("cm");
            em.persist(pUmMargenIzq);
        }

        TypedQuery<Parametro> tqmargenizq = em.createQuery("SELECT p FROM Parametro p WHERE p.clave='margenizq'", Parametro.class);
        List<Parametro> lstMargenIzq = tqmargenizq.getResultList();
        if (lstMargenIzq.isEmpty()) {
            Parametro pMargenIzq = new Parametro();
            pMargenIzq.setClave("margenizq");
            pMargenIzq.setValor("0.0");
            em.persist(pMargenIzq);
        }
        
        Parametro pvi=new Parametro();
        pvi.setClave("vistaprevia");
        pvi.setValor("false");
        em.merge(pvi);

        Cajero c = new Cajero();
        c.setIdCajero(1);
        c.setNombre("Juan Perez");
        em.merge(c);

        MedioPago mpEfe = new MedioPago();
        mpEfe.setIdMedioPago(1);
        mpEfe.setDescripcion("Efectivo");
        em.merge(mpEfe);

        MedioPago mpBono = new MedioPago();
        mpBono.setIdMedioPago(2);
        mpBono.setDescripcion("Bono");
        em.merge(mpBono);

        MedioPago mpExento = new MedioPago();
        mpExento.setIdMedioPago(3);
        mpExento.setDescripcion("Exento");
        em.merge(mpExento);

        em.getTransaction().commit();

        FXMLLoader loader = new FXMLLoader();
        Parent root = loader.load(getClass().getResourceAsStream("/fxml/VentanaLogin.fxml"));
        VentanaLoginController vlogContr = loader.getController();
        vlogContr.setStgPadre(stage);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Cobro de Peaje");
        stage.show();
//        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));
//        
//        Scene scene = new Scene(root);
//        scene.getStylesheets().add("/styles/Styles.css");
//        
//        stage.setTitle("JavaFX and Maven");
//        stage.setScene(scene);
//        stage.show();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
