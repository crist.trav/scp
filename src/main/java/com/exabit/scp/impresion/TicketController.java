package com.exabit.scp.impresion;

import com.exabit.scp.entidades.Parametro;
import com.exabit.scp.entidades.Transaccion;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 * FXML Controller class
 *
 * @author traver
 */
public class TicketController implements Initializable {

    @FXML
    private Text txCajero;
    @FXML
    private Text txFecha;
    @FXML
    private Text txNroTrans;
    @FXML
    private Text txCarril;
    @FXML
    private Text txCategoria;
    @FXML
    private Text txMetodoPago;
    @FXML
    private Text txTotal;
    @FXML
    private Text txPuesto;
    @FXML
    private Text txSeparador1;
    @FXML
    private Text txTituloCajero;
    @FXML
    private Text txTituloFecha;
    @FXML
    private Text txTituloNroTransaccion;
    @FXML
    private Text txTituloCarril;
    @FXML
    private Text txTituloCategoria;
    @FXML
    private Text txTituloMedioPago;
    @FXML
    private Text txTituloTotal;
    @FXML
    private Text txSeparador2;
    @FXML
    private Text txMopcRuc;
    @FXML
    private Text txSaludo;
    
        
    private final EntityManagerFactory emf=Persistence.createEntityManagerFactory("scpPU");
    List<Text> lstTxCambioTamanio = new ArrayList<>();
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.agregarTxALista();
        EntityManager em = this.emf.createEntityManager();
        
        TypedQuery<Parametro> qtam = em.createQuery("SELECT p FROM Parametro p WHERE p.clave = 'tamanioletraticket'", Parametro.class);
        List<Parametro> lstTamLetra = qtam.getResultList();
        if(!lstTamLetra.isEmpty()){
            Parametro pTamLet = lstTamLetra.get(0);
            try{
                Double tamLet = Double.parseDouble(pTamLet.getValor());
                for(Text t : this.lstTxCambioTamanio){
                    String fontFamily = t.getFont().getFamily();
                    FontWeight fontWeight = t.getFont().getStyle().equals("Bold") ? FontWeight.BOLD : FontWeight.NORMAL;
                    t.setFont(Font.font(fontFamily, fontWeight, tamLet));
                }
            }catch(NumberFormatException ex){
                System.err.println(ex);
                Alert al = new Alert(Alert.AlertType.WARNING);
                al.setTitle("Error");
                al.setHeaderText("Tamaño de letra inválido");
                al.setContentText("Se usará el tamaño de letra por defecto.");
                al.show();
            }
        }
    }

    private void agregarTxALista(){
        this.lstTxCambioTamanio.add(this.txCajero);
        this.lstTxCambioTamanio.add(this.txFecha);
        this.lstTxCambioTamanio.add(this.txNroTrans);
        this.lstTxCambioTamanio.add(this.txCarril);
        this.lstTxCambioTamanio.add(this.txCategoria);
        this.lstTxCambioTamanio.add(this.txMetodoPago);
        this.lstTxCambioTamanio.add(this.txTotal);
        this.lstTxCambioTamanio.add(this.txPuesto);
        //this.lstTxCambioTamanio.add(this.txSeparador1);
        //this.lstTxCambioTamanio.add(this.txSeparador2);
        this.lstTxCambioTamanio.add(this.txTituloCajero);
        this.lstTxCambioTamanio.add(this.txTituloCarril);
        this.lstTxCambioTamanio.add(this.txTituloCategoria);
        this.lstTxCambioTamanio.add(this.txTituloFecha);
        this.lstTxCambioTamanio.add(this.txTituloMedioPago);
        this.lstTxCambioTamanio.add(this.txTituloNroTransaccion);
        this.lstTxCambioTamanio.add(this.txTituloTotal);
        this.lstTxCambioTamanio.add(this.txMopcRuc);
        this.lstTxCambioTamanio.add(this.txSaludo);
    }
    
    public void cargarDatos(Transaccion t, String cajero, String carril, String metodoPago, String montoPago, String puesto){
        this.txCajero.setText(cajero);
        SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        this.txFecha.setText(sdf.format(t.getFechaHoraRegistro().getTime()));
        this.txNroTrans.setText(t.getIdTransaccion().toString());
        this.txCarril.setText(carril);
        this.txCategoria.setText(t.getCategoria());
        this.txMetodoPago.setText(metodoPago);
        this.txTotal.setText(montoPago + " Gs.");
        this.txPuesto.setText(puesto);
    }
    
}
