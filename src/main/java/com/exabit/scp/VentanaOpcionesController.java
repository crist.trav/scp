package com.exabit.scp;

import com.exabit.scp.entidades.Parametro;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.print.Printer;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 * FXML Controller class
 *
 * @author cristhian
 */
public class VentanaOpcionesController implements Initializable {
    
    private Stage stgPadre;

    @FXML
    private TextField txfDescripcionPuesto;
    @FXML
    private TextField txfPrecioCat1;
    @FXML
    private TextField txfPrecioCat2;
    @FXML
    private TextField txfPrecioCat3;
    @FXML
    private TextField txfPrecioCat4;
    @FXML
    private TextField txfPrecioCat5;
    @FXML
    private TextField txfPrecioCat6;
    @FXML
    private TextField txfPrecioCat7;
    @FXML
    private TextField txfPrecioCat8;
    @FXML
    private TextField txfPrecioCat9;
    @FXML
    private Button btnCerrarVentParam;
    @FXML
    private Button btnGuararParametros;
    @FXML
    private TextField txfMargenIzq;
    @FXML
    private ComboBox<String> cmbUnidadMedidaIzq;
    @FXML
    private TextField txfTamanioLetra;
    /**
     * Initializes the controller class.
     */
    
    private final EntityManagerFactory emf=Persistence.createEntityManagerFactory("scpPU");
    @FXML
    private TextField txfCarril;
    @FXML
    private ComboBox<Printer> cmbImpresoras;
    //private CheckBox chkVistaPrevia;
    @FXML
    private CheckBox chkMostrarPrincipal;
    @FXML
    private TextField txfNroInicialTransaccion;
    
    private final ObservableList<Printer> lstImpre = FXCollections.observableArrayList();
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.txfMargenIzq.focusedProperty().addListener((ObservableValue<? extends Boolean> obs, Boolean oldValue, Boolean newValue)->{
            if(!newValue){
                try{
                    Double margen=Double.parseDouble(txfMargenIzq.getText());
                }catch(Exception ex){
                    this.txfMargenIzq.setText("0.0");
                    System.out.println("Error al convertir margen a Double: "+ex.getMessage());
                }
            }
        });
        
        lstImpre.addAll(Printer.getAllPrinters());
        this.cmbImpresoras.setItems(lstImpre);
        this.cmbUnidadMedidaIzq.getItems().add("mm");
        this.cmbUnidadMedidaIzq.getItems().add("cm");
        this.cargarParametrosBd();
    }

    public Stage getStgPadre() {
        return stgPadre;
    }
 
    public void setStgPadre(Stage stgPadre) {
        this.stgPadre = stgPadre;
    }

    @FXML
    private void onActionBtnCerrarVenParam(ActionEvent event) {
        this.stgPadre.close();
    }

    @FXML
    private void onActionBtnGuardarParam(ActionEvent event) {
        this.guardarParamentros();
        Alert al=new Alert(AlertType.INFORMATION);
        al.setTitle("Guardar Parametros");
        al.setHeaderText("Éxito");
        al.setContentText("Reinicie la aplicacion.");
        al.show();
    }
    
    private void cargarParametrosBd(){
        EntityManager em=emf.createEntityManager();
        
        TypedQuery<Parametro> q=em.createQuery("SELECT p FROM Parametro p WHERE p.clave='carril'", Parametro.class);
        List<Parametro>lstPars=q.getResultList();
        if(!lstPars.isEmpty()){
           this.txfCarril.setText(lstPars.get(0).getValor());
        }
        TypedQuery<Parametro> qnp=em.createQuery("SELECT p FROM Parametro p WHERE p.clave='nombrepuesto'", Parametro.class);
        List<Parametro>lstNombrePuesto=qnp.getResultList();
        if(!lstNombrePuesto.isEmpty()){
            this.txfDescripcionPuesto.setText(lstNombrePuesto.get(0).getValor());
        }
        
        TypedQuery<Parametro> qp1=em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat1'", Parametro.class);
        List<Parametro>lstPrecioCat1=qp1.getResultList();
        if(!lstPrecioCat1.isEmpty()){
            this.txfPrecioCat1.setText(lstPrecioCat1.get(0).getValor());
        }
        TypedQuery<Parametro> qp2=em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat2'", Parametro.class);
        List<Parametro>lstPrecioCat2=qp2.getResultList();
        if(!lstPrecioCat2.isEmpty()){
            this.txfPrecioCat2.setText(lstPrecioCat2.get(0).getValor());
        }
        TypedQuery<Parametro> qp3=em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat3'", Parametro.class);
        List<Parametro>lstPrecioCat3=qp3.getResultList();
        if(!lstPrecioCat3.isEmpty()){
            this.txfPrecioCat3.setText(lstPrecioCat3.get(0).getValor());
        }
        TypedQuery<Parametro> qp4=em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat4'", Parametro.class);
        List<Parametro>lstPrecioCat4=qp4.getResultList();
        if(!lstPrecioCat4.isEmpty()){
            this.txfPrecioCat4.setText(lstPrecioCat4.get(0).getValor());;
        }
        TypedQuery<Parametro> qp5=em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat5'", Parametro.class);
        List<Parametro>lstPrecioCat5=qp5.getResultList();
        if(!lstPrecioCat5.isEmpty()){
            this.txfPrecioCat5.setText(lstPrecioCat5.get(0).getValor());
        }
        TypedQuery<Parametro> qp6=em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat6'", Parametro.class);
        List<Parametro>lstPrecioCat6=qp6.getResultList();
        if(!lstPrecioCat6.isEmpty()){
            this.txfPrecioCat6.setText(lstPrecioCat6.get(0).getValor());
        }
        TypedQuery<Parametro> qp7=em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat7'", Parametro.class);
        List<Parametro>lstPrecioCat7=qp7.getResultList();
        if(!lstPrecioCat7.isEmpty()){
            this.txfPrecioCat7.setText(lstPrecioCat7.get(0).getValor());
        }
        TypedQuery<Parametro> qp8=em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat8'", Parametro.class);
        List<Parametro>lstPrecioCat8=qp8.getResultList();
        if(!lstPrecioCat8.isEmpty()){
            this.txfPrecioCat8.setText(lstPrecioCat8.get(0).getValor());
        }
        TypedQuery<Parametro> qp9=em.createQuery("SELECT p FROM Parametro p WHERE p.clave='preciocat9'", Parametro.class);
        List<Parametro>lstPrecioCat9=qp9.getResultList();
        if(!lstPrecioCat9.isEmpty()){
            this.txfPrecioCat9.setText(lstPrecioCat9.get(0).getValor());
        }
        /*TypedQuery<Parametro> qvista=em.createQuery("SELECT p FROM Parametro p WHERE p.clave='vistaprevia'", Parametro.class);
        List<Parametro>lstVistaPrevia=qvista.getResultList();
        if(!lstVistaPrevia.isEmpty()){
            this.chkVistaPrevia.setSelected(Boolean.parseBoolean(lstVistaPrevia.get(0).getValor()));
        }*/
        TypedQuery<Parametro> qimpre=em.createQuery("SELECT p FROM Parametro p WHERE p.clave='impresora'", Parametro.class);
        List<Parametro>lstImpreBd=qimpre.getResultList();
        if(!lstImpreBd.isEmpty()){
            String nombreImpre=lstImpreBd.get(0).getValor();
            for(Printer p:this.lstImpre){
                if(p.getName().equals(nombreImpre)){
                    this.cmbImpresoras.getSelectionModel().select(p);
                    break;
                }
            }
        }
        
        TypedQuery<Parametro> tqpn=em.createQuery("SELECT p FROM Parametro p WHERE p.clave='nrotransaccion'", Parametro.class);
        List<Parametro> lstpnro=tqpn.getResultList();
        if(!lstpnro.isEmpty()){
            String nrotrans=lstpnro.get(0).getValor();
            this.txfNroInicialTransaccion.setText(nrotrans);
        }
        
        TypedQuery<Parametro> tqmostrarnrotrans=em.createQuery("SELECT p FROM Parametro p WHERE p.clave='mostrarnrotransaccionprincipal'", Parametro.class);
        List<Parametro> lstmostrarnrotrans=tqmostrarnrotrans.getResultList();
        if(!lstmostrarnrotrans.isEmpty()){
            String mostrarnrotrans=lstmostrarnrotrans.get(0).getValor();
            this.chkMostrarPrincipal.setSelected(Boolean.parseBoolean(mostrarnrotrans));
        }
        
        TypedQuery<Parametro> tqunidadizq=em.createQuery("SELECT p FROM Parametro p WHERE p.clave='ummargenizq'", Parametro.class);
        List<Parametro> lstumizq=tqunidadizq.getResultList();
        if(!lstumizq.isEmpty()){
            String strUmMargenIzq=lstumizq.get(0).getValor();
            this.cmbUnidadMedidaIzq.getSelectionModel().select(strUmMargenIzq);
        }else{
            this.cmbUnidadMedidaIzq.getSelectionModel().selectFirst();
        }
        
        TypedQuery<Parametro> tqmargenizq=em.createQuery("SELECT p FROM Parametro p WHERE p.clave='margenizq'", Parametro.class);
        List<Parametro>lstMargenIzq=tqmargenizq.getResultList();
        if(!lstMargenIzq.isEmpty()){
            this.txfMargenIzq.setText(lstMargenIzq.get(0).getValor());
        }else{
            this.txfMargenIzq.setText("0.0");
        }
        
        TypedQuery<Parametro> qtam = em.createQuery("SELECT p FROM Parametro p WHERE p.clave = 'tamanioletraticket'", Parametro.class);
        List<Parametro> lstTamLetra = qtam.getResultList();
        if(!lstTamLetra.isEmpty()){
            this.txfTamanioLetra.setText(lstTamLetra.get(0).getValor());
        }else{
            this.txfTamanioLetra.setText("9.0");
        }
        
    }
    
    private void guardarParamentros(){
        EntityManager em=this.emf.createEntityManager();
        em.getTransaction().begin();
        Parametro parNombre=new Parametro();
        parNombre.setClave("nombrepuesto");
        parNombre.setValor(this.txfDescripcionPuesto.getText());
        em.merge(parNombre);
        
        Parametro parCarril=new Parametro();
        parCarril.setClave("carril");
        parCarril.setValor(this.txfCarril.getText());
        em.merge(parCarril);
        
        Parametro pc1=new Parametro();
        pc1.setClave("preciocat1");
        pc1.setValor(this.txfPrecioCat1.getText());
        em.merge(pc1);
        
        Parametro pc2=new Parametro();
        pc2.setClave("preciocat2");
        pc2.setValor(this.txfPrecioCat2.getText());
        em.merge(pc2);
        
        Parametro pc3=new Parametro();
        pc3.setClave("preciocat3");
        pc3.setValor(this.txfPrecioCat3.getText());
        em.merge(pc3);
        
        Parametro pc4=new Parametro();
        pc4.setClave("preciocat4");
        pc4.setValor(this.txfPrecioCat4.getText());
        em.merge(pc4);
        
        Parametro pc5=new Parametro();
        pc5.setClave("preciocat5");
        pc5.setValor(this.txfPrecioCat5.getText());
        em.merge(pc5);
        
        Parametro pc6=new Parametro();
        pc6.setClave("preciocat6");
        pc6.setValor(this.txfPrecioCat6.getText());
        em.merge(pc6);
        
        Parametro pc7=new Parametro();
        pc7.setClave("preciocat7");
        pc7.setValor(this.txfPrecioCat7.getText());
        em.merge(pc7);
        
        Parametro pc8=new Parametro();
        pc8.setClave("preciocat8");
        pc8.setValor(this.txfPrecioCat8.getText());
        em.merge(pc8);
        
        Parametro pc9=new Parametro();
        pc9.setClave("preciocat9");
        pc9.setValor(this.txfPrecioCat9.getText());
        em.merge(pc9);
        
//        Parametro pvi=new Parametro();
//        pvi.setClave("vistaprevia");
//        pvi.setValor(this.chkVistaPrevia.isSelected()+"");
//        em.merge(pvi);
        
        Parametro impr=new Parametro();
        impr.setClave("impresora");
        impr.setValor(this.cmbImpresoras.getSelectionModel().getSelectedItem().getName());
        em.merge(impr);
        
        Parametro pnro=new Parametro();
        pnro.setClave("nrotransaccion");
        pnro.setValor(this.txfNroInicialTransaccion.getText());
        em.merge(pnro);
        
        Parametro pmostrarNroTrans=new Parametro();
        pmostrarNroTrans.setClave("mostrarnrotransaccionprincipal");
        pmostrarNroTrans.setValor(String.valueOf(this.chkMostrarPrincipal.isSelected()));
        em.merge(pmostrarNroTrans);
        
        Parametro pUmMargenIzq=new Parametro();
        pUmMargenIzq.setClave("ummargenizq");
        pUmMargenIzq.setValor(this.cmbUnidadMedidaIzq.getSelectionModel().getSelectedItem());
        em.merge(pUmMargenIzq);
        
        Parametro pMargenIzq=new Parametro();
        pMargenIzq.setClave("margenizq");
        pMargenIzq.setValor(this.txfMargenIzq.getText());
        em.merge(pMargenIzq);
        
        Parametro pTamLetra = new Parametro();
        pTamLetra.setClave("tamanioletraticket");
        pTamLetra.setValor(this.txfTamanioLetra.getText());
        em.merge(pTamLetra);
        
        em.getTransaction().commit();
    }
    
}
